# Bunder

#### How to Run
- `docker compose up -d`
- `make migrate_up`
- `make run`

#### Tech Debt
- Translate go validator validation error
- Close application gracefully
- Add some loggings
- Limit login attempts
- Better project structure

#### What Added
- Lint
- Deployment pipeline (Only test and build stages)