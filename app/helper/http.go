package helper

import (
	"encoding/json"
	"net/http"

	codederror "example.com/bunder/app/coded-error"
)

func WriteResponse(w http.ResponseWriter, responseBody interface{}, statusCode int) {
	responseBodyBytes, _ := json.Marshal(responseBody)

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(statusCode)
	_, _ = w.Write(responseBodyBytes)
}

func WriteErrorResponse(w http.ResponseWriter, err error) {
	statusCode := http.StatusInternalServerError
	errMessage := "internal server error"
	errDetail := err.Error()

	if codedErr, ok := err.(*codederror.CodedError); ok {
		switch codedErr.Code {
		case codederror.ErrorCodeBadRequest:
			statusCode = http.StatusBadRequest
		case codederror.ErrorCodeAuthorization:
			statusCode = http.StatusUnauthorized
		case codederror.ErrorCodeUnprocessableEntity:
			statusCode = http.StatusUnprocessableEntity
		}

		errMessage = codedErr.Message
		errDetail = codedErr.Detail
	}

	responseBody := map[string]map[string]string{
		"error": {
			"message": errMessage,
			"detail":  errDetail,
		},
	}

	WriteResponse(w, responseBody, statusCode)
}
