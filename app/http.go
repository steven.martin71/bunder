package app

import (
	"log"
	"net/http"

	"example.com/bunder/app/auth"
	"example.com/bunder/app/config"
	"github.com/go-chi/chi/v5"
)

func NewHTTPServer(config config.Config) {
	authController := auth.ProvideAuthController(config)

	router := chi.NewRouter()

	router.Post("/api/v1/users/register", authController.Register)
	router.Post("/api/v1/users/login", authController.Login)

	if err := http.ListenAndServe("localhost:3000", router); err != nil {
		log.Fatal(err)
	}
}
