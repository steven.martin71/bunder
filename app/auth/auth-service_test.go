package auth_test

import (
	"context"
	"testing"

	"example.com/bunder/app/auth"
	codederror "example.com/bunder/app/coded-error"
	"example.com/bunder/app/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
)

type AuthServiceImplTestSuite struct {
	suite.Suite
	ctrl           *gomock.Controller
	userRepository *mock.MockUserRepository
	passwordHasher *mock.MockPasswordHasher
	tokenIssuer    *mock.MockAuthTokenIssuer
	sut            *auth.AuthServiceImpl
}

func (s *AuthServiceImplTestSuite) SetupTest() {
	s.ctrl = gomock.NewController(s.T())
	s.userRepository = mock.NewMockUserRepository(s.ctrl)
	s.passwordHasher = mock.NewMockPasswordHasher(s.ctrl)
	s.tokenIssuer = mock.NewMockAuthTokenIssuer(s.ctrl)
	s.sut = auth.NewAuthServiceImpl(s.userRepository, s.passwordHasher, s.tokenIssuer)
}

func (s *AuthServiceImplTestSuite) AfterTest(suiteName, testName string) {
	s.ctrl.Finish()
}

func TestAuthServiceImpl(t *testing.T) {
	suite.Run(t, new(AuthServiceImplTestSuite))
}

func (s *AuthServiceImplTestSuite) TestLoginGivenInvalidInputShouldReturnUnprocessableEntityError() {
	dto := auth.LoginRequestDTO{
		Email:    "email",
		Password: "asdasd",
	}

	result, err := s.sut.Login(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnprocessableEntity, err.Code)
	s.Equal(auth.LoginResponseDTO{}, result)
}

func (s *AuthServiceImplTestSuite) TestLoginWhenUserNotFoundShouldReturnAuthorizationError() {
	dto := auth.LoginRequestDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	repositoryErr := codederror.NewCodedError(
		codederror.ErrorCodeEntityNotFound,
		"entity not found",
		"no user with the given email",
	)

	s.userRepository.EXPECT().GetByEmail(gomock.Any(), gomock.Any()).Return(nil, repositoryErr)

	result, err := s.sut.Login(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeAuthorization, err.Code)
	s.Equal(auth.LoginResponseDTO{}, result)
}

func (s *AuthServiceImplTestSuite) TestLoginOnUnexpectedGetUserErrorShouldReturnError() {
	dto := auth.LoginRequestDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	repositoryErr := codederror.NewCodedError(
		codederror.ErrorCodeUnexpectedServerError,
		"database error",
		"database connection is terminated",
	)

	s.userRepository.EXPECT().GetByEmail(gomock.Any(), gomock.Any()).Return(nil, repositoryErr)

	result, err := s.sut.Login(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnexpectedServerError, err.Code)
	s.Equal(auth.LoginResponseDTO{}, result)
}

func (s *AuthServiceImplTestSuite) TestLoginWhenPasswordDoesNotMatchHashShouldReturnAuthorizationError() {
	dto := auth.LoginRequestDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	s.userRepository.EXPECT().GetByEmail(gomock.Any(), gomock.Any()).Return(&auth.User{}, nil)
	s.passwordHasher.EXPECT().PasswordMatchesHash(gomock.Any(), gomock.Any()).Return(false)

	result, err := s.sut.Login(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeAuthorization, err.Code)
	s.Equal(auth.LoginResponseDTO{}, result)
}

func (s *AuthServiceImplTestSuite) TestLoginWhenGeneratingTokenErrorShouldReturnError() {
	dto := auth.LoginRequestDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	generateTokenErr := codederror.NewCodedError(
		codederror.ErrorCodeUnexpectedServerError,
		"unexpected error",
		"failed to generate token",
	)

	s.userRepository.EXPECT().GetByEmail(gomock.Any(), gomock.Any()).Return(&auth.User{}, nil)
	s.passwordHasher.EXPECT().PasswordMatchesHash(gomock.Any(), gomock.Any()).Return(true)
	s.tokenIssuer.EXPECT().GenerateToken(gomock.Any()).Return("", generateTokenErr)

	result, err := s.sut.Login(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnexpectedServerError, err.Code)
	s.Equal(auth.LoginResponseDTO{}, result)
}

func (s *AuthServiceImplTestSuite) TestLoginShouldReturnAccessToken() {
	dto := auth.LoginRequestDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	user := auth.User{
		ID:           "id",
		Email:        dto.Email,
		PasswordHash: "hash",
	}

	accessToken := "access_token"

	expectedGenerateTokenParam := map[string]interface{}{
		"user_id": user.ID,
		"email":   user.Email,
	}

	expectedResult := auth.LoginResponseDTO{
		AccessToken: accessToken,
	}

	s.userRepository.EXPECT().GetByEmail(gomock.Any(), gomock.Eq(dto.Email)).Return(&user, nil)
	s.passwordHasher.EXPECT().PasswordMatchesHash(gomock.Eq(dto.Password), gomock.Eq(user.PasswordHash)).Return(true)
	s.tokenIssuer.EXPECT().GenerateToken(gomock.Eq(expectedGenerateTokenParam)).Return(accessToken, nil)

	result, err := s.sut.Login(context.Background(), dto)

	s.Nil(err)
	s.Equal(expectedResult, result)
}
