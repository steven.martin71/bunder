package auth

import (
	"time"

	codederror "example.com/bunder/app/coded-error"
	"github.com/dgrijalva/jwt-go"
)

type JwtTokenIssuer struct {
	secret string
}

func NewJwtTokenIssuer(secret string) *JwtTokenIssuer {
	return &JwtTokenIssuer{
		secret: secret,
	}
}

func (i JwtTokenIssuer) GenerateToken(info map[string]interface{}) (string, *codederror.CodedError) {
	claims := jwt.MapClaims{}

	for key, value := range info {
		claims[key] = value
	}
	claims["exp"] = time.Now().Add(time.Hour * 1).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(i.secret))
	if err != nil {
		return "", codederror.NewCodedError(codederror.ErrorCodeUnexpectedServerError, "generate token failed", err.Error())
	}
	return tokenString, nil
}
