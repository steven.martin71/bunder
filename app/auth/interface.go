package auth

import (
	"context"

	codederror "example.com/bunder/app/coded-error"
)

//go:generate mockgen -source=interface.go -destination=../mock/auth_mock.go -package=mock

type UserService interface {
	CreateNewUser(ctx context.Context, dto UserRegistrationDTO) *codederror.CodedError
}

type AuthService interface {
	Login(ctx context.Context, dto LoginRequestDTO) (LoginResponseDTO, *codederror.CodedError)
}

type PasswordHasher interface {
	Hash(password string) (string, *codederror.CodedError)
	PasswordMatchesHash(password, hash string) bool
}

type AuthTokenIssuer interface {
	GenerateToken(info map[string]interface{}) (string, *codederror.CodedError)
}

type UserRepository interface {
	Create(ctx context.Context, user User) *codederror.CodedError
	GetByEmail(ctx context.Context, email string) (*User, *codederror.CodedError)
}
