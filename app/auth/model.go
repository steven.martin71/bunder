package auth

type UserRegistrationDTO struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginRequestDTO struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponseDTO struct {
	AccessToken string `json:"access_token"`
}

type User struct {
	ID           string
	Email        string
	PasswordHash string
}
