package auth

import (
	"context"

	codederror "example.com/bunder/app/coded-error"
	"github.com/go-playground/validator/v10"
)

type UserServiceImpl struct {
	userRepository UserRepository
	passwordHasher PasswordHasher
}

func NewUserServiceImpl(userRepository UserRepository, passwordHasher PasswordHasher) *UserServiceImpl {
	return &UserServiceImpl{
		userRepository: userRepository,
		passwordHasher: passwordHasher,
	}
}

func (s UserServiceImpl) CreateNewUser(ctx context.Context, dto UserRegistrationDTO) *codederror.CodedError {
	if err := s.validateUserRegistrationDTO(dto); err != nil {
		return codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "invalid request", err.Error())
	}

	passwordHash, err := s.passwordHasher.Hash(dto.Password)
	if err != nil {
		return codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "invalid request", err.Error())
	}

	user := User{
		Email:        dto.Email,
		PasswordHash: passwordHash,
	}

	if err := s.userRepository.Create(ctx, user); err != nil {
		if err.Code == codederror.ErrorCodeResourceAlreadyExists {
			return codederror.NewCodedError(err.Code, "confict", "email already exists")
		}
		return err
	}

	return nil
}

func (s UserServiceImpl) validateUserRegistrationDTO(dto UserRegistrationDTO) error {
	v := struct {
		Email    string `validate:"required,email"`
		Password string `validate:"required,min=6"`
	}{
		Email:    dto.Email,
		Password: dto.Password,
	}

	return validator.New().Struct(v)
}
