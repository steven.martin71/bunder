package auth

import (
	"context"

	codederror "example.com/bunder/app/coded-error"
	"github.com/go-playground/validator/v10"
)

type AuthServiceImpl struct {
	userRepository UserRepository
	passwordHasher PasswordHasher
	tokenIssuer    AuthTokenIssuer
}

func NewAuthServiceImpl(userRepository UserRepository, passwordHasher PasswordHasher, tokenIssuer AuthTokenIssuer) *AuthServiceImpl {
	return &AuthServiceImpl{
		userRepository: userRepository,
		passwordHasher: passwordHasher,
		tokenIssuer:    tokenIssuer,
	}
}

func (s AuthServiceImpl) Login(ctx context.Context, dto LoginRequestDTO) (LoginResponseDTO, *codederror.CodedError) {
	if err := s.validateLoginRequestDTO(dto); err != nil {
		return LoginResponseDTO{}, codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "invalid request", err.Error())
	}

	user, err := s.userRepository.GetByEmail(ctx, dto.Email)
	if err != nil {
		if err.Code != codederror.ErrorCodeEntityNotFound {
			return LoginResponseDTO{}, err
		}
		return LoginResponseDTO{}, codederror.NewCodedError(codederror.ErrorCodeAuthorization, "login failed", "invalid email or password")
	}

	if !s.passwordHasher.PasswordMatchesHash(dto.Password, user.PasswordHash) {
		return LoginResponseDTO{}, codederror.NewCodedError(codederror.ErrorCodeAuthorization, "login failed", "invalid email or password")
	}

	userInfo := map[string]interface{}{
		"user_id": user.ID,
		"email":   user.Email,
	}

	token, err := s.tokenIssuer.GenerateToken(userInfo)
	if err != nil {
		return LoginResponseDTO{}, err
	}

	result := LoginResponseDTO{
		AccessToken: token,
	}
	return result, nil
}

func (s AuthServiceImpl) validateLoginRequestDTO(dto LoginRequestDTO) error {
	v := struct {
		Email    string `validate:"required,email"`
		Password string `validate:"required"`
	}{
		Email:    dto.Email,
		Password: dto.Password,
	}

	return validator.New().Struct(v)
}
