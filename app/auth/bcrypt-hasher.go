package auth

import (
	codederror "example.com/bunder/app/coded-error"
	"golang.org/x/crypto/bcrypt"
)

type BcryptPasswordHasher struct{}

func NewBcryptPasswordHasher() *BcryptPasswordHasher {
	return &BcryptPasswordHasher{}
}

func (h BcryptPasswordHasher) Hash(password string) (string, *codederror.CodedError) {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "bad password", err.Error())
	}

	return string(passwordHash), nil
}

func (h BcryptPasswordHasher) PasswordMatchesHash(password, hash string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)); err != nil {
		return false
	}
	return true
}
