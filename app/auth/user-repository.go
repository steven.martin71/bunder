package auth

import (
	"context"
	"database/sql"

	codederror "example.com/bunder/app/coded-error"
	"github.com/google/uuid"
	"github.com/lib/pq"
)

type PostgresUserRepository struct {
	db *sql.DB
}

func NewPostgresUserRepository(db *sql.DB) *PostgresUserRepository {
	return &PostgresUserRepository{
		db: db,
	}
}

func (r PostgresUserRepository) Create(ctx context.Context, user User) *codederror.CodedError {
	query := `INSERT INTO users (id, email, password_hash) VALUES ($1, $2, $3);`

	user.ID = uuid.NewString()

	_, err := r.db.ExecContext(ctx, query, user.ID, user.Email, user.PasswordHash)
	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Code == "23505" {
				return codederror.NewCodedError(codederror.ErrorCodeResourceAlreadyExists, "entity already exists", err.Error())
			}
		}
		return codederror.NewCodedError(codederror.ErrorCodeUnexpectedServerError, "database error", err.Error())
	}

	return nil
}

func (r PostgresUserRepository) GetByEmail(ctx context.Context, email string) (*User, *codederror.CodedError) {
	query := `SELECT id, password_hash FROM users WHERE email = $1;`

	user := User{
		Email: email,
	}

	err := r.db.QueryRowContext(ctx, query, email).Scan(&user.ID, &user.PasswordHash)
	if err == sql.ErrNoRows {
		return nil, codederror.NewCodedError(codederror.ErrorCodeEntityNotFound, "entity not found", err.Error())
	}

	if err != nil {
		return nil, codederror.NewCodedError(codederror.ErrorCodeUnexpectedServerError, "database err", err.Error())
	}

	return &user, nil
}
