package auth_test

import (
	"testing"

	"example.com/bunder/app/auth"
	codederror "example.com/bunder/app/coded-error"
	"github.com/stretchr/testify/suite"
	"golang.org/x/crypto/bcrypt"
)

type BcryptPasswordHasherTestSuite struct {
	suite.Suite
	sut *auth.BcryptPasswordHasher
}

func (s *BcryptPasswordHasherTestSuite) SetupTest() {
	s.sut = auth.NewBcryptPasswordHasher()
}

func TestBcryptPasswordHasher(t *testing.T) {
	suite.Run(t, new(BcryptPasswordHasherTestSuite))
}

func (s *BcryptPasswordHasherTestSuite) TestHashGivenBadPasswordShouldReturnError() {
	password := "asdasdhisdhaisjdkajsdkajskjdaksjdkasjdkajdskdjaksdsjdiajshdiahsidhaishdiahsidahsishasdljaosjdaosijdoasjdoa"

	passwordHash, err := s.sut.Hash(password)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnprocessableEntity, err.Code)
	s.Len(passwordHash, 0)
}

func (s *BcryptPasswordHasherTestSuite) TestHashShouldReturnCorrectPasswordHash() {
	password := "asdasd"

	passwordHash, hashErr := s.sut.Hash(password)

	compareErr := bcrypt.CompareHashAndPassword([]byte(passwordHash), []byte(password))

	s.Nil(hashErr)
	s.NoError(compareErr)
}

func (s *BcryptPasswordHasherTestSuite) TestPasswordMatchesHashGivenRandomStringShouldReturnFalse() {
	password := "asdasd"
	hash := "asdihfaijfoa"

	match := s.sut.PasswordMatchesHash(password, hash)

	s.False(match)
}

func (s *BcryptPasswordHasherTestSuite) TestPasswordMatchesHashGivenMatchingHashShouldReturnTrue() {
	password := "asdasd"

	passwordHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	match := s.sut.PasswordMatchesHash(password, string(passwordHash))

	s.True(match)
}
