package auth_test

import (
	"context"
	"testing"

	"example.com/bunder/app/auth"
	codederror "example.com/bunder/app/coded-error"
	"example.com/bunder/app/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
)

type UserServiceImplTestSuite struct {
	suite.Suite
	ctrl           *gomock.Controller
	userRepository *mock.MockUserRepository
	passwordHasher *mock.MockPasswordHasher
	sut            *auth.UserServiceImpl
}

func (s *UserServiceImplTestSuite) SetupTest() {
	s.ctrl = gomock.NewController(s.T())
	s.userRepository = mock.NewMockUserRepository(s.ctrl)
	s.passwordHasher = mock.NewMockPasswordHasher(s.ctrl)
	s.sut = auth.NewUserServiceImpl(s.userRepository, s.passwordHasher)
}

func (s *UserServiceImplTestSuite) AfterTest(suiteName, testName string) {
	s.ctrl.Finish()
}

func TestUserServiceImpl(t *testing.T) {
	suite.Run(t, new(UserServiceImplTestSuite))
}

func (s *UserServiceImplTestSuite) TestCreateNewUserGivenInvalidInputShouldReturnUnprocessableEntityError() {
	dto := auth.UserRegistrationDTO{
		Email:    "email",
		Password: "asdasd",
	}

	err := s.sut.CreateNewUser(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnprocessableEntity, err.Code)
}

func (s *UserServiceImplTestSuite) TestCreateNewUserWhenPasswordHashingFailsShouldReturnUnprocessableEntityError() {
	dto := auth.UserRegistrationDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	passwordHashingErr := codederror.NewCodedError(
		codederror.ErrorCodeUnprocessableEntity,
		"bad password",
		"bad password",
	)

	s.passwordHasher.EXPECT().Hash(gomock.Any()).Return("", passwordHashingErr)

	err := s.sut.CreateNewUser(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnprocessableEntity, err.Code)
}

func (s *UserServiceImplTestSuite) TestCreateNewUserGivenEmailThatAlreadyExistsShouldReturnConflictError() {
	dto := auth.UserRegistrationDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	repositoryErr := codederror.NewCodedError(
		codederror.ErrorCodeResourceAlreadyExists,
		"conflict",
		"resource already exists",
	)

	s.passwordHasher.EXPECT().Hash(gomock.Any()).Return("passwordHash", nil)
	s.userRepository.EXPECT().Create(gomock.Any(), gomock.Any()).Return(repositoryErr)

	err := s.sut.CreateNewUser(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeResourceAlreadyExists, err.Code)
}

func (s *UserServiceImplTestSuite) TestCreateNewUserOnUnexpectedErrorShouldReturnError() {
	dto := auth.UserRegistrationDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	repositoryErr := codederror.NewCodedError(
		codederror.ErrorCodeUnexpectedServerError,
		"database error",
		"database connection is terminated",
	)

	s.passwordHasher.EXPECT().Hash(gomock.Any()).Return("passwordHash", nil)
	s.userRepository.EXPECT().Create(gomock.Any(), gomock.Any()).Return(repositoryErr)

	err := s.sut.CreateNewUser(context.Background(), dto)

	s.NotNil(err)
	s.Equal(codederror.ErrorCodeUnexpectedServerError, err.Code)
}

func (s *UserServiceImplTestSuite) TestCreateNewUserWhenUserIsCreatedSuccessfullyShouldReturnNoError() {
	dto := auth.UserRegistrationDTO{
		Email:    "email@g.com",
		Password: "asdasd",
	}

	passwordHash := "passwordHash"

	expectedUser := auth.User{
		Email:        dto.Email,
		PasswordHash: passwordHash,
	}

	s.passwordHasher.EXPECT().Hash(gomock.Any()).Return(passwordHash, nil)
	s.userRepository.EXPECT().Create(gomock.Any(), gomock.Eq(expectedUser)).Return(nil)

	err := s.sut.CreateNewUser(context.Background(), dto)

	s.Nil(err)
}
