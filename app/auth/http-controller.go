package auth

import (
	"encoding/json"
	"net/http"

	codederror "example.com/bunder/app/coded-error"
	"example.com/bunder/app/helper"
)

type AuthController struct {
	userService UserService
	authService AuthService
}

func NewAuthController(userService UserService, authService AuthService) *AuthController {
	return &AuthController{
		userService: userService,
		authService: authService,
	}
}

func (c AuthController) Register(w http.ResponseWriter, r *http.Request) {
	var dto UserRegistrationDTO

	if err := json.NewDecoder(r.Body).Decode(&dto); err != nil {
		codedErr := codederror.NewCodedError(codederror.ErrorCodeBadRequest, "invalid json", err.Error())
		helper.WriteErrorResponse(w, codedErr)
		return
	}

	if err := c.userService.CreateNewUser(r.Context(), dto); err != nil {
		helper.WriteErrorResponse(w, err)
		return
	}

	helper.WriteResponse(w, nil, http.StatusCreated)
}

func (c AuthController) Login(w http.ResponseWriter, r *http.Request) {
	var dto LoginRequestDTO

	if err := json.NewDecoder(r.Body).Decode(&dto); err != nil {
		codedErr := codederror.NewCodedError(codederror.ErrorCodeBadRequest, "invalid json", err.Error())
		helper.WriteErrorResponse(w, codedErr)
		return
	}

	response, err := c.authService.Login(r.Context(), dto)
	if err != nil {
		helper.WriteErrorResponse(w, err)
		return
	}

	helper.WriteResponse(w, response, http.StatusOK)
}
