package auth_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"example.com/bunder/app/auth"
	codederror "example.com/bunder/app/coded-error"
	"example.com/bunder/app/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
)

const (
	pathLogin string = "/api/v1/users/login"
)

type AuthControllerLoginTestSuite struct {
	suite.Suite
	ctrl        *gomock.Controller
	authService *mock.MockAuthService
	sut         *auth.AuthController
}

func (s *AuthControllerLoginTestSuite) SetupTest() {
	s.ctrl = gomock.NewController(s.T())
	s.authService = mock.NewMockAuthService(s.ctrl)
	s.sut = auth.NewAuthController((auth.UserService)(nil), s.authService)
}

func (s *AuthControllerLoginTestSuite) AfterTest(suiteName, testName string) {
	s.ctrl.Finish()
}

func TestAuthControllerLogin(t *testing.T) {
	suite.Run(t, new(AuthControllerLoginTestSuite))
}

func (s *AuthControllerLoginTestSuite) TestLoginGivenInvalidJsonPayloadShouldReturnBadRequest() {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathLogin, bytes.NewReader([]byte{}))

	s.sut.Login(w, r)

	s.Equal(http.StatusBadRequest, w.Result().StatusCode)
}

func (s *AuthControllerLoginTestSuite) TestLoginWhenRequestIsUnprocessableShouldReturnUnprocessableEntity() {
	request := map[string]string{
		"email":    "email",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathLogin, bytes.NewReader(requestBytes))
	err := codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "", "")

	s.authService.EXPECT().Login(gomock.Any(), gomock.Any()).Return(auth.LoginResponseDTO{}, err)

	s.sut.Login(w, r)

	s.Equal(http.StatusUnprocessableEntity, w.Result().StatusCode)
}

func (s *AuthControllerLoginTestSuite) TestLoginOnAuthorizationErrorShouldReturnUnauthorized() {
	request := map[string]string{
		"email":    "email",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathLogin, bytes.NewReader(requestBytes))
	err := codederror.NewCodedError(codederror.ErrorCodeAuthorization, "", "")

	s.authService.EXPECT().Login(gomock.Any(), gomock.Any()).Return(auth.LoginResponseDTO{}, err)

	s.sut.Login(w, r)

	s.Equal(http.StatusUnauthorized, w.Result().StatusCode)
}

func (s *AuthControllerLoginTestSuite) TestLoginWhenUnexpectedErrorHappensShouldReturnInternalServerError() {
	request := map[string]string{
		"email":    "email@asd.com",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathLogin, bytes.NewReader(requestBytes))
	err := codederror.NewCodedError(codederror.ErrorCodeUnexpectedServerError, "", "")

	s.authService.EXPECT().Login(gomock.Any(), gomock.Any()).Return(auth.LoginResponseDTO{}, err)

	s.sut.Login(w, r)

	s.Equal(http.StatusInternalServerError, w.Result().StatusCode)
}

func (s *AuthControllerLoginTestSuite) TestLoginWhenNewUserHasCreatedShouldReturnCreated() {
	request := map[string]string{
		"email":    "email@asd.com",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathLogin, bytes.NewReader(requestBytes))

	loginResponse := auth.LoginResponseDTO{
		AccessToken: "token",
	}

	expectedDto := auth.LoginRequestDTO{
		Email:    request["email"],
		Password: request["password"],
	}

	expectedResponse, _ := json.Marshal(map[string]string{"access_token": "token"})

	s.authService.EXPECT().Login(gomock.Any(), gomock.Eq(expectedDto)).Return(loginResponse, nil)

	s.sut.Login(w, r)

	response, _ := io.ReadAll(w.Result().Body)

	s.Equal(expectedResponse, response)
	s.Equal(http.StatusOK, w.Result().StatusCode)
}
