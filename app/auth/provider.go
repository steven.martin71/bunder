package auth

import (
	"example.com/bunder/app/config"
)

func ProvideAuthController(config config.Config) *AuthController {
	userRepository := NewPostgresUserRepository(config.DB)
	passwordHasher := NewBcryptPasswordHasher()
	tokenIssuer := NewJwtTokenIssuer(config.JwtSecret)
	userService := NewUserServiceImpl(userRepository, passwordHasher)
	authService := NewAuthServiceImpl(userRepository, passwordHasher, tokenIssuer)
	authController := NewAuthController(userService, authService)
	return authController
}
