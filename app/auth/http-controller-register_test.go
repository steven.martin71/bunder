package auth_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"example.com/bunder/app/auth"
	codederror "example.com/bunder/app/coded-error"
	"example.com/bunder/app/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
)

const (
	pathRegister string = "/api/v1/users/register"
)

type AuthControllerRegisterTestSuite struct {
	suite.Suite
	ctrl        *gomock.Controller
	userService *mock.MockUserService
	sut         *auth.AuthController
}

func (s *AuthControllerRegisterTestSuite) SetupTest() {
	s.ctrl = gomock.NewController(s.T())
	s.userService = mock.NewMockUserService(s.ctrl)
	s.sut = auth.NewAuthController(s.userService, (auth.AuthService)(nil))
}

func (s *AuthControllerRegisterTestSuite) AfterTest(suiteName, testName string) {
	s.ctrl.Finish()
}

func TestAuthControllerRegister(t *testing.T) {
	suite.Run(t, new(AuthControllerRegisterTestSuite))
}

func (s *AuthControllerRegisterTestSuite) TestRegisterGivenInvalidJsonPayloadShouldReturnBadRequest() {
	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathRegister, bytes.NewReader([]byte{}))

	s.sut.Register(w, r)

	s.Equal(http.StatusBadRequest, w.Result().StatusCode)
}

func (s *AuthControllerRegisterTestSuite) TestRegisterWhenRequestIsUnprocessableShouldReturnUnprocessableEntity() {
	request := map[string]string{
		"email":    "email",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathRegister, bytes.NewReader(requestBytes))
	err := codederror.NewCodedError(codederror.ErrorCodeUnprocessableEntity, "", "")

	s.userService.EXPECT().CreateNewUser(gomock.Any(), gomock.Any()).Return(err)

	s.sut.Register(w, r)

	s.Equal(http.StatusUnprocessableEntity, w.Result().StatusCode)
}

func (s *AuthControllerRegisterTestSuite) TestRegisterWhenUnexpectedErrorHappensShouldReturnInternalServerError() {
	request := map[string]string{
		"email":    "email@asd.com",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathRegister, bytes.NewReader(requestBytes))
	err := codederror.NewCodedError(codederror.ErrorCodeUnexpectedServerError, "", "")

	s.userService.EXPECT().CreateNewUser(gomock.Any(), gomock.Any()).Return(err)

	s.sut.Register(w, r)

	s.Equal(http.StatusInternalServerError, w.Result().StatusCode)
}

func (s *AuthControllerRegisterTestSuite) TestRegisterWhenNewUserHasCreatedShouldReturnCreated() {
	request := map[string]string{
		"email":    "email@asd.com",
		"password": "password",
	}
	requestBytes, _ := json.Marshal(request)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost, pathRegister, bytes.NewReader(requestBytes))

	expectedDto := auth.UserRegistrationDTO{
		Email:    request["email"],
		Password: request["password"],
	}

	s.userService.EXPECT().CreateNewUser(gomock.Any(), gomock.Eq(expectedDto)).Return(nil)

	s.sut.Register(w, r)

	s.Equal(http.StatusCreated, w.Result().StatusCode)
}
