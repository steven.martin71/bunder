package codederror

type ErrorCode int

const (
	ErrorCodeBadRequest            ErrorCode = 400
	ErrorCodeAuthorization         ErrorCode = 401
	ErrorCodeEntityNotFound        ErrorCode = 404
	ErrorCodeResourceAlreadyExists ErrorCode = 409
	ErrorCodeUnprocessableEntity   ErrorCode = 422
	ErrorCodeUnexpectedServerError ErrorCode = 500
)
