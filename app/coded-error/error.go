package codederror

import "fmt"

type CodedError struct {
	Code    ErrorCode
	Message string
	Detail  string
}

func (e CodedError) Error() string {
	return fmt.Sprintf("%s: %s", e.Message, e.Detail)
}

func NewCodedError(code ErrorCode, message, detail string) *CodedError {
	return &CodedError{
		Code:    code,
		Message: message,
		Detail:  detail,
	}
}
