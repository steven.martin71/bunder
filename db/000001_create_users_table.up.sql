CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    email VARCHAR(50) UNIQUE,
    password_hash VARCHAR(255) NOT NULL
);