package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"

	"example.com/bunder/app"
	"example.com/bunder/app/config"
)

func getDB() *sql.DB {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	pwd := os.Getenv("DB_PWD")
	dbname := os.Getenv("DB_NAME")

	connectionString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, pwd, dbname,
	)

	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal("Error opening database:", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("Error pinging database:", err)
	}
	fmt.Println("Connected to the database!")

	return db
}

func main() {
	config := config.Config{
		DB:        getDB(),
		JwtSecret: os.Getenv("JWT_SECRET"),
	}

	app.NewHTTPServer(config)
}
