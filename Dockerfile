FROM golang:1.20.6-alpine3.18 AS builder

RUN apk update && apk add --no-cache git

WORKDIR /app

COPY . .

RUN go mod tidy

RUN go build -o binary

FROM golang:1.20.6-alpine3.18

COPY --from=builder /app/binary ./

EXPOSE 3000

ENTRYPOINT [ "./binary" ]