include .env

migration_up:
	migrate -path db/ -database "postgresql://$(DB_USER):$(DB_PWD)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable" -verbose up

migration_down:
	migrate -path db/ -database "postgresql://$(DB_USER):$(DB_PWD)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)?sslmode=disable" -verbose down

lint:
	golangci-lint run

test:
	go test ./...

run:
	set -o allexport; source .env; set +o allexport && go run main.go